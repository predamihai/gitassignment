package ro.ase.csie.cts.assignment1;

public interface IGreetible {
	
	public void greet();
}
