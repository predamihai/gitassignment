package ro.ase.csie.cts.assignment1;

public class Chatbot implements IGreetible{
private String name;
private boolean isReady;
public Chatbot(String name, boolean isReady) {
	super();
	this.name = name;
	this.isReady = isReady;
}
@SuppressWarnings("unused")
private Chatbot()
{
	
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public boolean isReady() {
	return isReady;
}
public void setReady(boolean isReady) {
	this.isReady = isReady;
}

@Override
public void greet() {
	if(this.isReady==true)
	{
		System.out.println("Hi my name is "+this.name+" and i'm ready!");
	}
	else
	{
		System.out.println("Hi my name is "+this.name+" and i'm not ready...");
	}
	
}

}
